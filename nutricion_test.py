import unittest
from nutriente import Nutriente

class TestSequence(unittest.TestCase):

    def test_init01(self):
        # Comprueba que el constructor guarda los datos suministrados
        nut = Nutriente(150)
        self.assertEqual(nut.nutriente, 150)

if __name__ == "__main__":
    unittest.main()
