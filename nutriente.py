class Nutriente(object):
    """Una clase base que representa nutrientes.
    Cuidado: esta clase está sin terminar y se incluye sólo como modelo.
    :param n: El nutriente.
    :type n: int
    """

    def __init__(self, n: int):
       self.nutriente = n
  
    def calcula_calorias(self):
        """Calcula las calorias de todos los nutrientes."""
        pass

