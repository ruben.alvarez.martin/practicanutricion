# Practica Nutrición

Una práctica OOP para la asignatura PER usando conceptos basados en nutrición.

Entrega el día 8 de Abril con exámen el día 12 de Abril.

## Objetivo

El objetivo de la prática es diseñar un primer esqueleto de clases que usaremos para el tratamiento de nutrientes, alimentos, dietas, etc. En futuras prácticas iremos mejorando y creando un sistema mucho más complejo.

El alumno deberá entregar los siguientes elementos en un proyecto de gitlab:
-  Las clases en Python descritas más abajo
-  Una bateria de pruebas unitarias que comprueben el correcto funcionamiento de su código
-  Ficheros adicionales que considere necesarios. Por ejemplo, para activar las pruebas unitarias o para evitar subir a git ficheros auxiliares.
-   Un documento o Readme que describa el funcionamiento, principales característias, extras, etc. que considere oportuno destacar.

A continuación se describen estos elementos.

## Pasos previos a la práctica
Debe crear un proyecto privado en su cuenta de gitlab con el nombre de **"PER24-p1-Nutricion"**. **Es importante que mantenga ese nombre** ya que se utilizará para la corrección de la práctica.
El proyecto debe **ser privado y no visible** a ningún compañero, pero deben dar acceso a los profesores para su evaluación. Se pasarán comprobaciones para detectar la copia de código. Si se detecta que el proyecto contiene código y no es privado, el alumno **suspenderá automáticamente** la práctica.

Dentro de ese proyecto debe haber al menos siete ficheros llamados:
 -  "nutriente.py"
 -  "categoriaAlimento.py"
 -  "alimento.py" 
 -  "dieta.py"
 -  "dietaVegetariana.py"
 -  "dietaKeto.py"
 -  "unittests.py"
 
Los seis primeros serán el código en python que implementen las funcionalidades abajo escritas, en el último estarán los tests que comprueben el correcto funcionamiento de la práctica.
Se recomienda que se añadan los ficheros que configura Gitlab para la integración continua (CI/CD)
En el proyecto podría haber otros ficheros adicionales tales como un documento explicando el funcionamiento de la práctica (readme) o respondiendo a las preguntas de los profesores, así como un fichero que controle la subida de ficheros no deseados.

## Contexto
El aumento de la conciencia sobre la importancia de una alimentación saludable ha llevado a un creciente interés en el seguimiento y la gestión de la información nutricional. En esta práctica, desarrollarás un programa en Python que permita a los usuarios gestionar información sobre alimentos, menús y dietas, proporcionando un análisis detallado de su contenido nutricional.

## Ficheros
El alumno deberá modelar 7 ficheros python con los nombres descritos anteriormente.

### Nutriente
En este fichero se representará nutrientes (por ejemplo: proteínas, carbohidratos, grasas, fibra, colesterol...).  

Debe cumplir con los siguiente requisitos:
- Tiene que tener los atributos suficientes para almacenar los valores de los distintos nutrientes.
- Debe tener un constructor para inicializar los atributos.
- Debe proporcionar métodos para establecer y obtener los valores de cada nutriente.
- Debe proporcionar cuantas calorías tiene según los valores de los nutrientes aportados. Más información en [Calculadora calorias](https://www.tuasaude.com/es/calorias-de-los-alimentos/)

### CategoriaAlimento
En este fichero se representará las diferentes categorías de alimentos, como frutas, verduras, carnes, lácteos, etc. Más información: https://docs.python.org/3/library/enum.html

### Alimento
En este fichero se representará un alimento y contendrá los siguientes atributos:

Debe cumplir con los siguiente requisitos:
- Debe contener atributos para almacenar el nombre del alimento, así como un atributo para almacenar el objeto Nutrientes asociado al alimento.
- Debe tener un atributo para almacenar la categoría a la que pertenece el alimento.
- Debe proporcionar un constructor para inicializar estos atributos.

### Dieta 
Representará una dieta diaria y contendrá una serie de alimentos que el usuario debe seguir durante la semana.

Debe cumplir con los siguiente requisitos:
- Debe tener un constructor.
- Debe contener los atributos suficientes para almacenar los alimentos que componen una dieta.
- Debe proporcionar métodos para agregar un menú a la dieta, calcular el contenido nutricional de la dieta completa y mostrar la dieta con su contenido nutricional.
- Debe proporcionar métodos para obtener los alimentos de determinado día de la semana. 
- Debe evaluar la dieta total en función de las calorias y el metabolismo basal. Hay diferentes fórmulas que se pueden emplear, de más sencilla a más compleja ([Más información](https://www.vitonica.com/dietas/calcula-tu-dieta-paso-a-paso-calcular-necesidades-caloricas-i)): 
    - **Básica:** 
        - MB Hombre: 1 caloría x peso en Kg. x 24 Horas
        - MB Mujer: 0,9 calorías x peso en Kg. x 24 Horas
    - **Fórmula Harris-Benedict:** 
        - MB Hombre: 66,473 + (13,751 x peso en kg) + (5,0033 x estatura en cm) - (6,7550 x edad en años) 
        - MB Mujer: 655,1 + (9,463 x peso en kg) + (1,8 x estatura en cm) - (4,6756 x edad en años)
    - **Fórmula Katch-McArdle:** 370 + (21,6 * Masa Corporal Magra en kg). La masa corporal magra es el peso corporal menos el peso de la grasa corporal.

    Estas ecuaciones dan como resultado la cantidad total de calorías de mantenimiento, el método debe validar si la dieta se aleja demasiado de esa cifra o no. Es decir, no hace falta que las calorías de la dieta sea exactamente las mismas que el obtenido en la fórmula. Se puede emplear un rango de valores

### DietaVegetariana
En este fichero se representará una dieta vegetariana, que se caracteriza por excluir alimentos de origen animal.

Debe cumplir con los siguientes requisitos: 
- Debe tener un método que compruebe que no hay ningún alimento de origen animal en la dieta. 
- Debe añadir un atributo que indique los suplementos necesarios para la dieta (si necesita suplementación de B12, omega3...).

### DietaKeto
En este fichero se representará una dieta cetogénica, que es un enfoque alimenticio que se caracteriza por ser baja en carbohidratos y alta en grasas, lo que induce al cuerpo a entrar en un estado metabólico llamado cetósis.

Debe cumplir con los siguientes requisitos: 
- Un método que devuelva si la dieta cetógenica es correcta o no. Para ello, los porcentajes de nutrientes que debe respetar esta dieta son los siguientes: 75% de las calorías en forma de grasas, 20% de de las calorías diarias en forma de proteína y 5% de las calorías diarias en forma de carbohidratos. [Más información](https://www.ketoconlaura.com/blogs/biblioteca-keto/c-mo-leer-etiquetas-en-keto).

## Pruebas básicas
Adicionalmente al código, el alumno deberá desarrollar su propia batería de tests unitarios. En la corrección de la práctica se evaluará el correcto desarrollo del código y la calidad de las pruebas. Es decir, la nota se pondrá en función de la calidad del código, así como el número y calidad de los tests realizados. No es necesario hacer tests unitarios de los getters y setters de cada clase, sólo de los métodos y funciones que tengan cierta funcionalidad. 

Se estima que será necesario un mínimo de unos 10-15 tests.

## Documentación

El alumno deberá incluir en el Readme un texto que explique las siguientes cuestiones:
-   ¿Ha usado herencia? ¿Dónde? Justifica las razones de haberla usado o no.
-   ¿Has contemplado en tu diseño aspectos de visibilidad de atributos? Desarrolla este concepto e indica algún punto de tu código en el que se observe su uso (de haberlo usado).
-   ¿Alguna clase tiene referencias a otra clase? Es decir, ¿alguna clase tiene un atributo que sea una referencia a una instancia de otra clase?
-   ¿Es lo mismo funciones que métodos?  Justifica tú respuesta.  ¿Has usado métodos y/o funciones en tu código?. ¿Dónde? Responde diciendo exactamente en qué fichero y en qué línea lo has usado.
-   ¿Es lo mismo referencias que instancias?  Justifica muy bien tu respuesta. Indica algún punto de tu código en el que se creen instancias. Indica alguno en el que se copien o modifiquen referencias. 

## Para recuperar este proyecto
Recuerda que para obtener una copia del proyecto puedes realizar el siguiente comando en tu disco:

```
cd existing_repo
git clone https://gitlab.eif.urjc.es/ruben.alvarez.martin/practicanutricion.git
```

Sin embargo esto solo te hará una copia que internamente tiene referencias a mi proyecto. Si haces cambios no podrás subirlo a tu proyecto. Solo es útil para poder usar los ficheros que os incluyo. NO modifiques ese directorio.
